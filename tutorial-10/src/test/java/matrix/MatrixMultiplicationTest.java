package matrix;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

public class MatrixMultiplicationTest {
    //TODO Implement, apply your test cases here
    double[][] first = {{2,3}, {1,4}};
    double[][] second = {{5,1}, {2,4}};
    double[][] third = {{1,2,3}};
    double[][] expectedSquaredResult = {{16, 14}, {13, 17}};
    double[][] result;

    @Test
    public void testSquareBasicMultiplication() throws InvalidMatrixSizeForMultiplicationException {
        result = MatrixOperation.basicMultiplicationAlgorithm(first, second);
        assertTrue(Arrays.deepEquals(result, expectedSquaredResult));
    }

    @Test
    public void testSquareStrassensMultiplication() {
        result = MatrixOperation.strassenMatrixMultiForNonSquareMatrix(first, second);
        assertTrue(Arrays.deepEquals(result, expectedSquaredResult));
    }

    @Test(expected = InvalidMatrixSizeForMultiplicationException.class)
    public void testNonSquareBasicMultipicationError()
            throws InvalidMatrixSizeForMultiplicationException {
        result = MatrixOperation.basicMultiplicationAlgorithm(first, third);
    }

}
