package sorting;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

public class SortAndSearchTest {
    //TODO Implement, apply your test cases here
    int[] arr = {4,1,2,7,6,10,8,9,3,5};

    @Test
    public void testSorter() {
        int[] slowSorted = Sorter.slowSort(arr);
        int[] fastSorted = Sorter.fastSort(arr);
        assertTrue(Arrays.equals(slowSorted, fastSorted));
    }

    @Test
    public void testFinder() {
        int slowFinder = Finder.slowSearch(arr, 7);
        int fastFinder = Finder.fastSearch(arr, 7);
        assertTrue(slowFinder == fastFinder);
    }

}
