package sorting;


public class Sorter {

    /**
     * Some sorting algorithm that possibly the slowest algorithm.
     *
     * @param inputArr array of integer that need to be sorted.
     * @return a sorted array of integer.
     */

    public static int[] slowSort(int[] inputArr) {
        int temp;
        for (int i = 1; i < inputArr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (inputArr[j] < inputArr[j - 1]) {
                    temp = inputArr[j];
                    inputArr[j] = inputArr[j - 1];
                    inputArr[j - 1] = temp;
                }
            }
        }
        return inputArr;
    }

    public static int[] fastSort(int[] arr) {
        int low = 0;
        int x = low;
        int high = arr.length - 1;
        int y = high;
        int pivotIndex = (low + high) / 2;
        int pivot = arr[pivotIndex];

        while (x <= y) {
            while (arr[x] < pivot) {
                x++;
            }
            while (arr[y] > pivot) {
                y--;
            }
            if (x <= y) {
                swap(x, y, arr);
                x++;
                y--;
            }
        }
        return arr;
    }

    public static void swap(int x, int y, int[] arr) {
        int temp = arr[x];
        arr[x] = arr[y];
        arr[y] = temp;
    }


}
