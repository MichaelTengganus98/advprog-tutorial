package matrix;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    private static String genericMatrixPath = "plainTextDirectory/input/matrixProblem";
    private static String pathFileMatrix1 = genericMatrixPath + "A/matrixProblemSet1.txt";
    private static int numberOfLine1 = 50;

    private static String pathFileMatrix2 = genericMatrixPath + "A/matrixProblemSet2.txt";
    private static int numberOfLine2 = 50;

    //for not square
    private static String pathFileMatrix3 = genericMatrixPath + "B/matrixProblemSet3.txt";
    private static int numberofRow3 = 5;
    private static int numberOfColumn3 = 10;

    private static String pathFileMatrix4 = genericMatrixPath + "B/matrixProblemSet4.txt";
    private static int numberofRow4 = 10;
    private static int numberOfColumn4 = 5;

    public static void main(String[] args) throws
            IOException, InvalidMatrixSizeForMultiplicationException {

        //Convert into array
        double[][] firstMatrix = convertInputFileToMatrix(pathFileMatrix1,
                numberOfLine1, numberOfLine1);
        double[][] secondMatrix = convertInputFileToMatrix(pathFileMatrix2,
                numberOfLine2, numberOfLine2);

        //TODO Implement, do your benchmark test for these algorithm start from here

        //Example usage of basic multiplication algorithm.
        long totalMilisMultiplication = System.currentTimeMillis();
        double[][] multiplicationResult =
                MatrixOperation.basicMultiplicationAlgorithm(firstMatrix, secondMatrix);
        totalMilisMultiplication = System.currentTimeMillis() - totalMilisMultiplication;
        System.out.println(" Basic Multiplication Complete in "
                + totalMilisMultiplication + " milisecond");


        //Example usage of strassen multiplication algorithm.
        long totalMilisStrassenMultiplication = System.currentTimeMillis();
        double[][] strassenMultiplicationResult =
                MatrixOperation.strassenMatrixMultiForNonSquareMatrix(firstMatrix, secondMatrix);
        totalMilisStrassenMultiplication = System.currentTimeMillis()
                - totalMilisStrassenMultiplication;
        System.out.println("Strassen Multiplication Complete in "
                + totalMilisStrassenMultiplication + " milisecond");

        double[][] thirdMatrix = convertInputFileToMatrix(pathFileMatrix3,
                numberofRow3, numberOfColumn3);
        double[][] forthMatrix = convertInputFileToMatrix(pathFileMatrix4,
                numberofRow4, numberOfColumn4);

        long totalMilisStrassenBMultiplication = System.currentTimeMillis();
        double[][] strassenBMultiplicationResult =
                MatrixOperation.strassenMatrixMultiForNonSquareMatrix(thirdMatrix, forthMatrix);
        totalMilisStrassenBMultiplication = System.currentTimeMillis()
                - totalMilisStrassenBMultiplication;
        System.out.println("Strassen Multiplication Matrix B Complete in "
                + totalMilisStrassenBMultiplication + " milisecond");

    }

    /**
     * Converting a file input into an 2 dimensional array of double that represent a matrix.
     * @param pathFile is a path to file input.
     * @param numberOfLine the number of row (and possibly column) inside the square matrix.
     * @return 2 dimensional array of double representing matrix.
     * @throws IOException in the case of the file is not found because of the wrong path of file.
     */
    private static double[][] convertInputFileToMatrix(String pathFile,
                                                       int numberOfRow, int numberOfColumn)
            throws IOException {
        File matrixFile = new File(pathFile);
        FileReader fileReader = new FileReader(matrixFile);
        double[][] matrix = new double[numberOfRow][numberOfColumn];

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String currentLine;
        int indexOfLine = 0;
        while ((currentLine = bufferedReader.readLine()) != null) {
            matrix[indexOfLine] = sequenceIntoArray(currentLine);
            indexOfLine++;
        }
        return matrix;
    }

    /**
     * Converting a row of sequence of double into an array.
     * @param currentLine sequence of double from input representing a row from matrix.
     * @return array of double representing a row from matrix.
     */
    private static double[] sequenceIntoArray(String currentLine) {
        String[] arrInput = currentLine.split(" ");
        double[] arrInteger = new double[arrInput.length];
        for (int index = 0; index < arrInput.length; index++) {
            arrInteger[index] = Double.parseDouble(arrInput[index]);
        }
        return arrInteger;
    }
}
