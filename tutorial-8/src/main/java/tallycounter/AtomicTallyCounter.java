package tallycounter;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicTallyCounter {
    private AtomicInteger counter = new AtomicInteger(0);

    public void increment() {
        counter.getAndIncrement();//counter++;
    }

    public void decrement() {
        counter.getAndDecrement();//counter--;
    }

    public AtomicInteger value() {
        return counter;
    }
}
