import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class CustomerTest {

    private Customer customer;
    private Movie movie;
    private Rental rent;

    @Before
    public void setUp() {
        customer = new Customer("Alice");
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rent = new Rental(movie, 3);
    }
    
    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    // TODO Implement me!
    public void statementWithMultipleMovies() {
        // TODO Implement me!
        Movie movie1 = new Movie("Spongebob Squarepants The Movie", Movie.CHILDREN);
        Rental rent1 = new Rental(movie1, 1);
        
        Movie movie2 = new Movie("Avengers Infinity War", Movie.NEW_RELEASE);
        Rental rent2 = new Rental(movie2, 2);
        
        customer.addRental(rent1);
        customer.addRental(rent2);
        String result = customer.statement();
        String[] lines = result.split("\n");
        
        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 11.0"));
        assertTrue(result.contains("3 frequent renter points"));
    }
    
    @Test
    public void htmlStatementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.htmlStatement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("<P>You owe <EM> 3.5"));
        assertTrue(result.contains("On this rental you earned 1"));
    }

}