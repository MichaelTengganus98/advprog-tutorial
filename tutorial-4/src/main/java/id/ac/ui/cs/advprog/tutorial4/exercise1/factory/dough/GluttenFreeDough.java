package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class GluttenFreeDough implements Dough {
    public String toString() {
        return "Glutten Free style health dough";
    }
}
