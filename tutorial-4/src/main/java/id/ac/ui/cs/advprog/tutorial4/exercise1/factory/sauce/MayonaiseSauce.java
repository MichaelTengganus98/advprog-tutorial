package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class MayonaiseSauce implements Sauce {
    public String toString() {
        return "Mayonaise sauce";
    }
}
