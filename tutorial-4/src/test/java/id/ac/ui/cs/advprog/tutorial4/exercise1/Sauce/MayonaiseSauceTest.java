package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MayonaiseSauceTest {

    private MayonaiseSauce sauce;

    @Before
    public void setUp() throws Exception {
        sauce = new MayonaiseSauce();
    }

    @Test
    public void testThisIsMayonaiseSauce() {
        assertEquals("" + sauce, "Mayonaise sauce");
    }
}
