package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class GluttenFreeDoughTest {

    private GluttenFreeDough dough;

    @Before
    public void setUp() throws Exception {
        dough = new GluttenFreeDough();
    }

    @Test
    public void testThisIsGluttenFreeDough() {
        assertEquals("" + dough, "Glutten Free style health dough");
    }
}
