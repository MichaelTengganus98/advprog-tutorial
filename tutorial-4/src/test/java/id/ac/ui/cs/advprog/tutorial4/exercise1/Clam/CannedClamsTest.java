package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CannedClamsTest {

    private CannedClams clam;

    @Before
    public void setUp() throws Exception {
        clam = new CannedClams();
    }

    @Test
    public void testThisIsCannedClams() {
        assertEquals("" + clam, "Canned Clams from Chesapeake Bay");
    }
}
