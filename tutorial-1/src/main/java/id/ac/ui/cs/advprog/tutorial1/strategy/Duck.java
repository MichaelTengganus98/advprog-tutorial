package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    FlyBehavior flyBehavior;
    QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    // TODO Complete me!
	public abstract void display();
	
    public void swim() {
		
    }

    public void setFlyBehavior(FlyBehavior param) {
        flyBehavior = param;
    }

    public void setQuackBehavior(QuackBehavior param) {
        quackBehavior = param;
    }
}
