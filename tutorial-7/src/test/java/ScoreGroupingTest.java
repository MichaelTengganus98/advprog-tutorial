import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {
    Map<String, Integer> scores = new HashMap<>();

    @Before
    public void setUp(){
        scores.put("Alice", 1);
        scores.put("Bob", 1);
        scores.put("Charlie", 3);
        scores.put("Delta", 2);
        scores.put("Emi", 2);
        scores.put("Foxtrot", 5);   
    }
    
    @Test
    public void testForScoreGroupingTrue() {
        String expectedOutput = "{1=[Bob, Alice], 2=[Emi, Delta], 3=[Charlie], 5=[Foxtrot]}";
        assertEquals(expectedOutput, ScoreGrouping.groupByScores(scores).toString());
    }
}