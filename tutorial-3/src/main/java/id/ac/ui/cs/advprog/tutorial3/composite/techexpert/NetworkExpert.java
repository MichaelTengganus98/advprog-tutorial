package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class NetworkExpert extends Employees {
    //TODO Implement	
    static final double minSalary = 50000.00;
    public NetworkExpert(String name,double salary) {
        this.name = name;
        this.role = "Network Expert";
        if (salary<this.minSalary) {
            throw new IllegalArgumentException();
        }
        else{
            this.salary = salary;
        }
    }
    
    @Override
    public double getSalary(){
        return this.salary;
    }
}
