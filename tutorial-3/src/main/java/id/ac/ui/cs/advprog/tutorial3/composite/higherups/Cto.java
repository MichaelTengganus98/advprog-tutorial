package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    static final double minSalary = 100000.00;
    public Cto(String name, double salary) {
        //TODO Implement
        this.name = name;
        this.role = "CTO";
        if (salary<this.minSalary) {
            throw new IllegalArgumentException();
        }
        else{
            this.salary = salary;
        }
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return this.salary;
    }
}
