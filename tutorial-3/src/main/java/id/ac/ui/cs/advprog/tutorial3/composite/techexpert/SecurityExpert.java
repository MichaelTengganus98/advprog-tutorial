package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class SecurityExpert extends Employees {
    //TODO Implement	
    static final double minSalary = 70000.00;
    public SecurityExpert(String name,double salary) {
        this.name = name;
        this.role = "Security Expert";
        if (salary<this.minSalary) {
            throw new IllegalArgumentException();
        }
        else{
            this.salary = salary;
        }
    }
    
    @Override
    public double getSalary(){
        return this.salary;
    }
}

