package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class BackendProgrammer extends Employees {
    //TODO Implement	
    static final double minSalary = 20000.00;
    public BackendProgrammer(String name,double salary) {
        this.name = name;
        this.role = "Back End Programmer";
        if (salary<this.minSalary) {
            throw new IllegalArgumentException();
        }
        else{
            this.salary = salary;
        }
    }
    
    @Override
    public double getSalary(){
        return this.salary;
    }
}
